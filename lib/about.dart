import 'package:suka_sakit/appBar.dart';
import 'package:suka_sakit/colorPick.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      body: Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.center,
          color: Warna.grey,
          child: Column(
            children: <Widget>[
              Center(
                  child: Text(
                "Toko Suka sakit berdiri sejak tahun 1926 yang didirikan oleh sekelompok mahasiswa lulusan akademi kedokteran, dengan usaha dan kegigihan perlahan toko ini semakin maju dan mulai menyebar keseluruh nusantara. Toko suka sakit didirikan untuk memenuhi kebutuhan masyarakat yang mengalami kekurangan fisik dan penyakit fisik",
                style: TextStyle(color: Colors.black, fontSize: 17),
                textAlign: TextAlign.justify,
              )),
              SizedBox(
                height: 50,
              ),
              Text(
                "Copyright Ponca Hadi, 18282015",
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          )),
    ));
  }
}
