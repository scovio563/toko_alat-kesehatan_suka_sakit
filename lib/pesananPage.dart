import 'package:flutter/material.dart';
import 'package:suka_sakit/appBar.dart';

import 'viewModel.dart';

class PesananPage extends StatefulWidget {
  @override
  _PesananPageState createState() => _PesananPageState();
}

class _PesananPageState extends State<PesananPage> {
  List dataProduk = new List();

  void getDataPesanan() {
    UserViewModel().getPesanan().then((value) {
      setState(() {
        dataProduk = value;
      });
    });
  }

  @override
  void initState() {
    getDataPesanan();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      body: _body(),
    ));
  }

  Widget _body() {
    return ListView.builder(
        itemCount: dataProduk.length,
        itemBuilder: (context, i) {
          return Container(
            child: Row(
              children: [
                Container(
                    padding: EdgeInsets.all(5.0),
                    width: 150,
                    child: Text(
                      dataProduk[i]['nama'],
                      style: TextStyle(fontSize: 20),
                    )),
                Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Text("Rp. " + dataProduk[i]['harga'])),
              ],
            ),
          );
        });
  }
}
