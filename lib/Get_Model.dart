import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id;
  String firstName;
  String email, last_name, avatar;

  UserGet({this.id, this.firstName, this.email, this.last_name, this.avatar});

  factory UserGet.createUserGet(Map<String, dynamic> object) {
    return UserGet(
        id: object['id'].toString(),
        firstName: object['first_name'],
        email: object['email'],
        last_name: object['last_name'],
        avatar: object['avatar']);
  }

  static Future<UserGet> connectToApiUser(String id) async {
    String apiURLPOST = 'http://reqres.in/api/users/' + id;
    var apiResult = await http.get(apiURLPOST);

    var jsonobject = json.decode(apiResult.body);

    var userData = (jsonobject as Map<String, dynamic>)['data'];

    return UserGet.createUserGet(userData);
  }
}
