import 'package:flutter/material.dart';
import 'package:suka_sakit/home.dart';

import 'pesananPage.dart';

class ViewhomePage extends StatefulWidget {
  ViewhomePage({Key key}) : super(key: key);

  @override
  _ViewhomePageState createState() => _ViewhomePageState();
}

class _ViewhomePageState extends State<ViewhomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 200,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (context) {
                    return BerandaPage();
                  }));
                },
                padding: EdgeInsets.all(12),
                color: Colors.lightBlueAccent,
                child:
                    Text('Stok Produk', style: TextStyle(color: Colors.white)),
              ),
            ),
            Container(
              width: 200,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (context) {
                    return PesananPage();
                  }));
                },
                padding: EdgeInsets.all(12),
                color: Colors.lightBlueAccent,
                child: Text('Pesanan Produk',
                    style: TextStyle(color: Colors.white)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
