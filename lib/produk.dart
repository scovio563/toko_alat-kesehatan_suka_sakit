import 'package:suka_sakit/appBar.dart';
import 'package:suka_sakit/colorPick.dart';
import 'package:flutter/material.dart';
import 'package:suka_sakit/home.dart';

import 'jsonModel.dart';
import 'viewModel.dart';

class Produk extends StatefulWidget {
  @override
  _ProdukState createState() => _ProdukState();
}

class _ProdukState extends State<Produk> {
  TextEditingController _nama = TextEditingController();
  TextEditingController _harga = TextEditingController();
  TextEditingController _gambar = TextEditingController();

  void addData() {
    UserpostModel commRequest = UserpostModel();
    commRequest.nama = _nama.text;
    commRequest.harga = _harga.text;
    commRequest.gambar = _gambar.text;

    UserViewModel()
        .postUser(userpostModelToJson(commRequest))
        .then((value) => print('success'));
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _form()),
    );
  }

  Widget _form() {
    return new Column(
      children: <Widget>[
        new ListTile(
          leading: const Icon(Icons.paste_rounded),
          title: new TextField(
            controller: _nama,
            decoration: new InputDecoration(
              hintText: "Nama Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.money),
          title: new TextField(
            controller: _harga,
            decoration: new InputDecoration(
              hintText: "Harga Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.image),
          title: new TextField(
            controller: _gambar,
            decoration: new InputDecoration(
              hintText: "Gambar Url produk",
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        new InkWell(
          onTap: () async {
            addData();

            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context) {
                  return AlertDialog(
                    title: const Text('Pemberitahuan'),
                    content: SingleChildScrollView(
                      child: ListBody(
                        children: const <Widget>[
                          Text('Tambah Data Dikirim'),
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Approve'),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => BerandaPage()));
                        },
                      ),
                    ],
                  );
                });
          },
          child: Container(
            width: 200,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Warna.orenMuda, Warna.merahKeterangan])),
            child: Text(
              'Tambah Produk',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
