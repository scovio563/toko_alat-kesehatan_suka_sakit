import 'package:flutter/material.dart';
import 'package:suka_sakit/appBar.dart';

import 'Get_Model.dart';

class DataDiri extends StatefulWidget {
  @override
  _DataDiriState createState() => _DataDiriState();
}

class _DataDiriState extends State<DataDiri> {
  UserGet userGet;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserGet.connectToApiUser('3').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AllAppBar(),
          body: Center(
            child: Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.blue[100],
              alignment: Alignment.center,
              padding: EdgeInsets.all(30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "WELCOME!",
                    style: TextStyle(fontSize: 48, color: Color(0xff0563BA)),
                  ),
                  Container(
                      child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(70.0),
                        child: Image.network(
                          (userGet != null)
                              ? userGet.avatar
                              : "https://64.media.tumblr.com/d14d75dba6f12d7bf63bb93b38cbbf8f/tumblr_pt6g4hSPyu1sk269go1_1280.jpg",
                          cacheHeight: 128,
                          cacheWidth: 128,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          (userGet != null)
                              ? userGet.firstName + " " + userGet.last_name
                              : "Data Gagal Diambil",
                          style:
                              TextStyle(fontSize: 24, color: Color(0xff0563BA)),
                        ),
                      ),
                      Text(
                        (userGet != null)
                            ? userGet.email
                            : "Data gagal Diambil",
                        style: TextStyle(fontSize: 18, color: Colors.black),
                      )
                    ],
                  ))
                ],
              ),
            ),
          )),
    );
  }
}
