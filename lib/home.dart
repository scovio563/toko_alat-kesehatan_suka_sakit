import 'package:suka_sakit/appBar.dart';
import 'package:flutter/material.dart';

import 'jsonModel.dart';
import 'pesananPage.dart';
import 'viewModel.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  List dataProduk = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataProduk = value;
      });
    });
  }

  @override
  void initState() {
    getDataUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: grid()),
    );
  }

  Widget grid() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      itemCount: dataProduk.length,
      itemBuilder: (context, i) {
        return Container(
          child: Card(
            color: Color.fromARGB(255, 255, 177, 41),
            child: Column(
              children: [
                Image.network(
                  dataProduk[i]['gambar'] ??
                      "https://upload.wikimedia.org/wikipedia/commons/d/d2/Stethoscope-2.png",
                  width: 80,
                ),
                Text(dataProduk[i]['nama'] ?? "data Nama Gagal Diambil",
                    style: TextStyle(fontSize: 20)),
                Text(dataProduk[i]['harga'] ?? "data gambar gagal diambil",
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                Padding(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: [
                        RaisedButton(
                          onPressed: () {
                            Pesanan commRequest = Pesanan();
                            commRequest.id = dataProduk[i]['id'];

                            UserViewModel()
                                .pesanan(pesananpostModelToJson(commRequest))
                                .then((value) => print('success'));

                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => PesananPage()));
                          },
                          child: Row(
                            children: [
                              Icon(Icons.add_shopping_cart),
                              Text("Add")
                            ],
                          ),
                        ),
                        RaisedButton(
                          onPressed: () {},
                          child: Row(
                            children: [Icon(Icons.star), Text("fav")],
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
        );
      },
    );
  }
}
