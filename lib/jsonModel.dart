import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());
String pesananpostModelToJson(Pesanan data) => json.encode(data.toPesan());

class UserpostModel {
  UserpostModel({this.nama, this.harga, this.gambar});

  String nama;
  String harga, gambar;

  Map toJson() => {
        "nama": nama,
        "harga": harga,
        "gambar": gambar,
      };
}

class Pesanan {
  String id;
  Pesanan({this.id});

  Map toPesan() => {
        "id": id,
      };
}
